CREATE DATABASE rodo;
USE rodo;

CREATE TABLE passageiro (
 id_passageiro int(11) NOT NULL,
 nome varchar(100) NOT NULL,
 gênero tinyint(1) NOT NULL,
 rg varchar(15) NOT NULL,
 cpf varchar(20) NOT NULL,
 endereço varchar(100) NOT NULL,
 email varchar(200) NOT NULL,
 telefone varchar(15) NOT NULL
);