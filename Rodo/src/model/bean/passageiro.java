package model.bean;

public class passageiro {
	
	private int id_passageiro;
	private String nome;
	private boolean g�nero;
	private String rg;
	private String cpf;
	private String endere�o;
	private String email;
	private String telefone;
	
	
	public int getId_passageiro() {
		return id_passageiro;
	}
	public void setId_passageiro(int id_passageiro) {
		this.id_passageiro = id_passageiro;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getG�nero() {
		return g�nero;
	}
	public void setG�nero(Boolean g�nero) {
		this.g�nero = g�nero;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEndere�o() {
		return endere�o;
	}
	public void setEndere�o(String endere�o) {
		this.endere�o = endere�o;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	

}
